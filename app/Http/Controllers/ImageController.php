<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function list(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'provider',
            2 =>'url',
            2 =>'action',
        );

		$limit = $request->input('length');
		$start = $request->input('start');
		$dir = $request->input('order.0.dir');
		$order = $columns[$request->input('order.0.column')];

        $images = Image::offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();

        $totalData = Image::all()->count();
		$totalFiltered = $totalData;
        
		$data = array();
        foreach($images as $image) {
            $nestedData['id'] = $image->id;
            $nestedData['provider'] = $image->provider;
            $nestedData['url'] = $image->url;
            $nestedData['action'] = '
                <button class="btnEdit btn btn-primary" data-id="' . $image->id . '" title="Edit Image"><i class="fa fa-edit"></i></button>
                <button class="btnDelete btn btn-danger" data-id="' . $image->id . '" title="Delete Image"><i class="fa fa-trash"></i></button>
                <button class="btnView btn btn-success" data-id="' . $image->id . '" title="View Image"><i class="fa fa-eye"></i></button>
            ';

            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),  
            "recordsTotal" => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data" => $data,
            "request" => $request->all()
        );

        return json_encode($json_data);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('images');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            Image::create([
                'provider' => $request->provider,
                'url' => $request->url
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Image $image)
    {
        return Image::findOrFail($image->id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Image $image)
    {
        try {
            $image = Image::findOrFail($image->id);
            $image->provider = $request->provider;
            $image->url = $request->url;
            $image->save();

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Image $image)
    {
        try {
            $image = Image::findOrFail($image->id);
            $image->delete();

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }
}
