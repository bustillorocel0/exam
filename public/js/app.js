var editedImage = '';
var dataTable = $('#dataTable').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax":{
        "url": "/images/list",
        "dataType": "json",
        "type": "GET",
    },
    "order":[[0,'desc']],
    "rowId": 'id',
    "searching": false,
    "columns": [
        { "data": "id" },
        { "data": "provider" },
        { "data": "url" },
        { "data": "action" },
    ] 
});

$('#btnAdd').click(function() {
    $('#addForm').show();
    $('#editForm').hide();
});

$('#addForm').submit(function(e) {
    e.preventDefault();
    let provider = $('#txtName').val();
    let url = $('#txtURL').val();

    $.ajax({
        url: url,
        method: 'get',
        dataType: 'json',
        success: function (response) {
            obj = { 'provider': provider, 'url': response.message };

            addDataProvider(obj);
        }
    });
    
    $(this).trigger("reset");
});

$('#editForm').submit(function(e) {
    e.preventDefault();
    let provider = $('#editName').val();
    let url = $('#editURL').val();
    let id = $('#imageID').val();

    if(editedImage != url) {
        $.ajax({
            url: url,
            method: 'get',
            dataType: 'json',
            success: function (response) {
                obj = { 'provider': provider, 'url': response.message };
    
                editDataProvider(id, obj);
            }
        });
    }
    else {
        obj = { 'provider': provider, 'url': url };

        editDataProvider(id, obj);
    }
});

$(document).on('click','.btnEdit', function () {
    let id = $(this).data('id');
    $('#imageID').val(id);
    $('#editForm').show();
    $('#addForm').hide();

    $.ajax({
        url: 'images/' + id,
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $('#editName').val(response.provider);
            $('#editURL').val(response.url);
            $('#dataProviderModal').modal('show');
            editedImage = response.url;
        }
    });
});

$(document).on('click','.btnDelete', function () {
    let id = $(this).data('id');
    $('#deleteID').val(id);
    $('#deleteModal').modal('show');
});

$('#confirmDelete').click(function() {
    let id = $('#deleteID').val();

    $.ajax({
        url: 'images/' + id,
        method: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            dataTable.draw();
            alert(response.status);
            $('#deleteModal').modal('hide');
        }
    });
});

$(document).on('click','.btnView', function () {
    let id = $(this).data('id');

    $.ajax({
        url: 'images/' + id,
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $('#viewImageModal').modal('show');
            $('#image').attr('src', response.url);
        }
    });
});

function addDataProvider(obj) {
    $.ajax({
        url: 'images',
        method: 'POST',
        data: obj,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            dataTable.draw();
            alert(response.status);
            $('#dataProviderModal').modal('hide');
        }
    });
}

function editDataProvider(id, obj) {
    $.ajax({
        url: 'images/' + id,
        method: 'PUT',
        data: obj,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            dataTable.draw();
            alert(response.status);
            $('#dataProviderModal').modal('hide');
        }
    });
}